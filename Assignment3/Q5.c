#include <stdio.h>
int main(void)
{
    int num, p, q;

    printf("Enter the number: ");
    scanf("%d", &num);

    for(p = 1; p <= num; p++)
    {
        for (q = 1; q <=10; q++)
        {
            printf("%d * %d = %d\n", p, q, p*q);
        }
        printf("-------------------------\n");
    }
return 0;
}
