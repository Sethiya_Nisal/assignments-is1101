#include <stdio.h>
int main(void)
{
  int num, reve = 0;

  printf("Enter the number:\n");
  scanf("%d", &num);

  while (num != 0)
  {
    reve = reve * 10;
    reve = reve + num % 10;
    num = num / 10;
  }

  printf("Reversed Number = %d\n", reve);
return 0;
}
