#include <stdio.h>
int main(void){
    int num;
    printf("Enter the number:\n");
    scanf("%d", &num);
    if(num % 2 == 0)
        printf("%d is an even number.", num);
    else
        printf("%d is an odd number.", num);
    
    return 0;
}