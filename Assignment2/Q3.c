#include <stdio.h>
 
int main(void)
{
    char c;
     
    printf("Enter a English letter: ");
    scanf("%c",&c);
     switch(c)
        {
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                printf("%c is a VOWEL.\n",c);
                break;
            default:
                printf("%c is a CONSONANT.\n",c); 
            }
            return 0;
}
