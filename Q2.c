#include <stdio.h>
int main(void){
	float radius, area;
	const float pi=3.14;
	printf("Enter radius of the disk\n");
	scanf("%f", &radius);
	area= pi * radius * radius;
	printf("Area of the disk= %f\n", area);
	return 0;
}